cmake_minimum_required(VERSION 3.10)
project(wapi C)

set(CMAKE_C_STANDARD 11)

add_library(wapi wapi.h util.c network.c wireless.c util.h)

FIND_PACKAGE(PkgConfig)

PKG_CHECK_MODULES(libnl-3.0 REQUIRED libnl-3.0>=3.1.0)

include_directories(/usr/include/libnl3/)

TARGET_LINK_LIBRARIES(wapi /usr/lib/libnl-3.so  libnl-genl-3.so)

install (TARGETS wapi DESTINATION bin )
install (FILES wapi.h DESTINATION include)
